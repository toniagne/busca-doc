@extends('layouts.site')
@section('title', 'Encontre empresas e profissionais')
@section('content')
<main>
    <div class="container">
        <div id="wizard_container">
            <form name="example-1" id="wrapped" method="POST">
                <input id="website" name="website" type="text" value="">
                <!-- Leave for security protection, read docs for details -->
                <div id="middle-wizard">
                    <div class="step">
                        <div class="question_title">
                            <h3>{{ $contents[0]->name }}</h3>
                            <p>{{ $contents[0]->legend }}</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-4">
                                <div class="item">
                                    <input id="answer_1" type="radio" name="answers_1[]" value="perdeu" class="required answer_1">
                                    <label for="answer_1"><img src="img/seo_icon_1.svg" alt=""><strong>PERDI !</strong>Documentos & Objetos.</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="item">
                                    <input id="answer_2" name="answers_1[]" type="radio" value="achou" class="required answer_1">
                                    <label for="answer_2"><img src="img/web_development_icon_1.svg" alt=""><strong>ACHEI !</strong>Documentos & Objetos.</label>
                                </div>
                            </div>
                        </div>
                        <!-- /row-->
                    </div>
                    <!-- /step-->

                    <div class="step">
                        <div class="question_title">
                            <h3>Que tipo de item você <span id='segment'></span> ?</h3>
                            <p>Selecione um item abaixo</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-4">
                                <div class="item">
                                    <input id="answer_1_group_1" type="radio" name="answer_group_1" value="Company" class="required">
                                <label for="answer_1_group_1"><img src="img/company_icon.svg" alt=""><strong>{{ $contents[1]->name }}</strong>{{ $contents[1]->legend }}</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="item">
                                    <input id="answer_2_group_1" name="answer_group_1" type="radio" value="Private User" class="required">
                                    <label for="answer_2_group_1"><img src="img/private_icon.svg" alt=""><strong>{{ $contents[2]->name }}</strong>{{ $contents[2]->legend }}</label>
                                </div>
                            </div>
                        </div>
                        <!-- /row-->
                    </div>
                    <!-- /step -->

                    <div class="step">
                        <div class="question_title">
                            <h3>Formulário de cadastro </h3>
                            <p>Preencha o formulário</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="box_general">
                                    <div class="form-group select">
                                        <label>Formulário de opções:</label>
                                        <div class="styled-select">
                                            <select class="required" name="select_1">
                                                <option value="" selected>Select</option>
                                                <option value="Unix/Linux + Mysql">Unix/Linux + Mysql</option>
                                                <option value="Windows + Sql">Windows + Sql</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /select-->
                                    <div class="form-group select">
                                        <label>Opções:</label>
                                        <div class="styled-select">
                                            <select class="required" name="select_2">
                                                <option value="" selected>Select</option>
                                                <option value="Hosting Plan 1 year + Mysql database 500MB">1 year + Mysql database 500MB</option>
                                                <option value="Hosting Plan 2 year + Mysql database 500MB">2 year + Mysql database 500MB</option>
                                                <option value="Hosting Plan 2 year + Mysql database 1GB">2 year + Mysql database 1GB</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /select-->
                                    <div class="form-group select">
                                        <label>Opção :</label>
                                        <div class="styled-select">
                                            <select class="required" name="select_3">
                                                <option value="" selected>Select</option>
                                                <option value="Mailchimp">Mailchimp</option>
                                                <option value="CampaignMonitor">CampaignMonitor</option>
                                                <option value="MailUp">MailUp</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /select-->
                                </div>
                                <!-- /box_general -->
                            </div>
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /step-->
                    
                    <!-- Budget ============================== -->
                    <div class="step">
                        <div class="question_title">
                            <h3>Recompensa ?</h3>
                            <p>Arraste para dar a recompensa</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="box_general">
                                    <div class="rounded_slider">
                                        <div id="budget_slider" style="margin: 0 auto 20px;"></div>
                                        <p>Eu sed epicuri mentitum, ex mei hinc justo, no cum dictas deserunt gubergren. Ferri pericula sententiae eu pro.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /Budget ============================== -->
                    
                    <!-- Last step ============================== -->
                    <div class="submit step">
                        <div class="question_title">
                            <h3>Detalhes </h3>
                            <p>Ei duo homero postea dignissim.</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="box_general">
                                    <div class="form-group">
                                        <input type="text" name="first_last_name" class="required form-control" placeholder="First and Last name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" class="required form-control" placeholder="Your Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="telephone" class="form-control" placeholder="Your Telephone">
                                    </div>
                                    <div class="form-group add_bottom_30">
                                        <div class="styled-select">
                                            <select class="required" name="country">
                                                <option value="" selected>Select your country</option>
                                                <option value="Europe">Europe</option>
                                                <option value="Asia">Asia</option>
                                                <option value="North America">North America</option>
                                                <option value="South America">South America</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="checkbox_questions">
                                        <input name="terms" type="checkbox" class="icheck required" value="yes">
                                        <label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a>.</label>
                                    </div>
                                </div>
                                <!-- /box_general -->
                            </div>
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /Last step ============================== -->
                </div>
                <!-- /middle-wizard -->
                <div id="bottom-wizard">
                    <button type="button" name="backward" class="backward">Anterior </button>
                    <button type="button" name="forward" class="forward">Próximo</button>
                    <button type="submit" name="process" class="submit">Enviar</button>
                </div>
                <!-- /bottom-wizard -->
            </form>
        </div>
        <!-- /Wizard container -->
    </div>
    <!-- /Container -->
</main>
<!-- /main -->
 @push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
   $('input[type=radio]').change(function() {   
      $('#segment').text($(".answer_1:checked").val());
   });
});
   </script>
@endpush

@endsection