<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class SiteController extends Controller
{
    public function index(){
        return view('site.index', [
            'contents' => Content::All()
        ]);
    }
}
